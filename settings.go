package main

import (
	"io/ioutil"
	"log"
	"os"

	"gopkg.in/yaml.v2"
)

// Conf get configuration data from ghost.yaml
type Conf struct {
	DocumentRoot string `yaml:"document-root"`
	Port         string `yaml:"port"`
	Root         string `yaml:"root"`

	// NotImplementedYet
	// Read             bool   `yaml:"read"`
	// Write            bool   `yaml:"write"`
	// WatchFileChanges bool   `yaml:"watch-file-changes"`
	// Authentication struct   `yaml:"auth"`
	// Proxy struct   `yaml:"proxy"`
}

func (c *Conf) getConf() *Conf {

	rootDir, _ := os.Getwd()
	// defautl location for ghost.config is going to be the root directory of where ghost is run,
	// however user's can override this behavior by setting the environment variable GHOST_CONFIG_PATH
	ghostConfigPath := os.Getenv("GHOST_CONFIG_PATH")
	if ghostConfigPath == "" {
		ghostConfigPath = rootDir + "/ghost.yaml"
	}
	if _, err := os.Stat(ghostConfigPath); err == nil {
		// ghost.yaml exists, use it's configuration
		log.Println("loading config: ", ghostConfigPath)
		yamlFile, err := ioutil.ReadFile(ghostConfigPath)
		if err != nil {
			log.Printf("yamlFile.Get err   #%v ", err)
		}
		err = yaml.Unmarshal(yamlFile, c)
		if err != nil {
			log.Fatalf("Unmarshal: %v", err)
		}

	} else if os.IsNotExist(err) {
		// ghost.yaml does *not* exist or couldn't be found
		log.Println("ghost does not exist or cannot be found, using defaults")
		c.DocumentRoot = "/usr/src/app"
		c.Port = "8080"
		c.Root = "index.html"

	} else {
		// Schrodinger: file may or may not exist. See err for details.

		// Therefore, do *NOT* use !os.IsNotExist(err) to test for file existence
	}

	// environment variables should override any perviously set variable, make sure this is always last
	c.Port = getEnv("GHOST_PORT", c.Port)
	c.Root = getEnv("GHOST_ROOT", c.Root)
	c.DocumentRoot = getEnv("GHOST_DOCUMENT_ROOT", c.DocumentRoot)

	return c
}

// since this tests for length of the string value and the empty string "" has length 0
//it means that when the env variable is set to "" you would get the fallback/default
// value rather than the value (which is trivially "").
func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}
