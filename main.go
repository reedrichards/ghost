package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"mime"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

// Document used to represent file
//
// Name is the path to the file
// Content is the content of the file
type Document struct {
	Name    string
	Content []byte
}

// Documents a document is a file that we want to serve
var Documents = make(map[string]Document)

func fileData(filePath string) ([]byte, error) {

	file, err := ioutil.ReadFile(filePath)
	if err != nil {
		return nil, err
	}
	return file, nil
}

// inserts document into memory given relative path to document
func insertDocument(path string, data []byte) {
	var doc Document
	doc.Name = path
	doc.Content = data
	log.Println("insterting: ", path)
	Documents[doc.Name] = doc
}

// loadDocuments into var Documents recursively from root directory
// called to intialize memory store
// TODO make concurrent
func loadDocuments(docRoot string) {
	fileList := []string{}
	err := filepath.Walk(docRoot, func(path string, f os.FileInfo, err error) error {
		if !f.IsDir() {
			fileList = append(fileList, path)
		}
		return nil
	})
	if err != nil {
		fmt.Println(err)
	}

	// TODO make this concurrent
	for _, file := range fileList {
		// need to strip filepath of absolute path so that lookups work in view
		name := after(file, docRoot+"/")
		content, _ := fileData(file)
		insertDocument(name, content)
	}
}

// Get substring after a string.
func after(value string, a string) string {
	pos := strings.LastIndex(value, a)
	if pos == -1 {
		return ""
	}
	adjustedPos := pos + len(a)
	if adjustedPos >= len(value) {
		return ""
	}
	return value[adjustedPos:len(value)]
}

func serveDocuments(w http.ResponseWriter, r *http.Request, rootDoc string) {
	var response []byte
	var contentType string
	path := r.URL.Path[1:]
	log.Println("visted: ", Documents[path].Name)
	if path != "" {
		response = Documents[path].Content
		contentType = mimeTypeForFile(path)
	} else {
		response = Documents[rootDoc].Content
		contentType = mimeTypeForFile(rootDoc)
	}
	fmt.Println("Content Type: " + contentType)
	w.Header().Set("Content-Type", contentType)

	fmt.Fprintf(w, string(response))

}

// needs there to be a /etc/mime.types directory
func mimeTypeForFile(file string) string {
	// We use a built in table of the common types since the system
	// TypeByExtension might be unreliable. But if we don't know, we delegate
	// to the system.
	ext := filepath.Ext(file)
	log.Println("looking up mimetype for extension: ", ext)
	return mime.TypeByExtension(ext)
}

func main() {
	log.Println("starting ghost")

	var c Conf
	fmt.Println("loading conf")
	c.getConf()
	fmt.Println("loaded conf: ", c)

	fmt.Println("loading documents")
	docRoot := c.DocumentRoot
	loadDocuments(docRoot)
	fmt.Println("loaded documents")

	//
	done := make(chan bool)

	//
	go func() {

		fmt.Println("watching files")
		watchFiles(docRoot)

	}()

	go func() {

		fmt.Println("starting webserver")
		rootDoc := c.Root
		http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			serveDocuments(w, r, rootDoc)
		})
		port := ":" + c.Port
		log.Fatal(http.ListenAndServe(port, nil))

	}()
	<-done

}
