package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"

	"github.com/fsnotify/fsnotify"
)

//
var watcher *fsnotify.Watcher

// main
func watchFiles(docRoot string) {

	// creates a new file watcher
	watcher, _ = fsnotify.NewWatcher()
	defer watcher.Close()

	// starting at the root of the project, walk each file/directory searching for
	// directories
	if err := filepath.Walk(docRoot, watchDir); err != nil {
		fmt.Println("ERROR", err)
	}

	//
	done := make(chan bool)

	//
	go func() {
		withSlashDocRoot := docRoot + "/"
		for {
			select {
			// watch for events
			case event := <-watcher.Events:
				switch event.Op {
				case fsnotify.Create:
					// insert documents need's relative path to file, not absolute
					path := replaceNth(event.Name, withSlashDocRoot, "", 1)
					content, _ := fileData(event.Name)
					log.Println("Created: ", path)
					insertDocument(path, content)
				case fsnotify.Write:
					path := replaceNth(event.Name, withSlashDocRoot, "", 1)
					content, _ := fileData(event.Name)
					log.Println("Wrote: ", path)
					insertDocument(path, content)
				case fsnotify.Remove:
					path := replaceNth(event.Name, withSlashDocRoot, "", 1)
					log.Println("Removed: ", path)
					delete(Documents, path)
					// TODO make a log syntax delineating removing from fs and memory
					log.Println("Removed: ", path)
				}
				fmt.Printf("EVENT! %s\n", event.String())
				// watch for errors
			case err := <-watcher.Errors:
				fmt.Println("ERROR", err)
			}
		}
	}()

	<-done
}

// watchDir gets run as a walk func, searching for directories to add watchers to
func watchDir(path string, fi os.FileInfo, err error) error {

	// since fsnotify can watch all the files in a directory, watchers only need
	// to be added to each nested directory
	if fi.Mode().IsDir() {
		return watcher.Add(path)
	}

	return nil
}

// Replace the nth occurrence of old in s by new.
func replaceNth(s, old, new string, n int) string {
	i := 0
	for m := 1; m <= n; m++ {
		x := strings.Index(s[i:], old)
		if x < 0 {
			break
		}
		i += x
		if m == n {
			return s[:i] + new + s[i+len(old):]
		}
		i += len(old)
	}
	return s
}
