# gHost

<!--
Not implemented yet, but planning for the future
###
### Read bool
### Write bool
### WatchFileChanges bool
### Authentication struct
authentication configuration
### Proxy str
allows you to proxy to a tcp connection, loading whatever is at that location into memory

-->


a read only in memory file server written in go

## Installation

## Confgiuration
### resolution order

1. Default setting
2. ghost.yaml
3. environment variables
### Configuring with yaml
#### document-root string `yaml:"document-root"`
> Default: "/usr/src/app"
root directory where documents are located.
#### port         int    `yaml:"port"`
> Default: "8080"
port gHost will serve documents from
#### root         string `yaml:"root"`
> Default: "index.html"
root document to serve on "/"
#### sample ghost.yaml
```yaml
document-root: "/mnt/data/pgns"
port: "8081"
root: "index.html"
```
### Configuring with environment variables
environment variables are essentially all caps versions of there yaml correspondents, prefixed with `GHOST_`
#### `GHOST_PORT`
port to serve on
#### `GHOST_DOCUMENT_ROOT`
root directory where documents are located.
#### `GHOST_ROOT`
relative path to root document to serve on "/"

---
The following environment variable configuration will share the directory `/mnt/data/pgns` on port `8082` using `/mnt/data/pgns/hello-world.txt` as the root documents

```
GHOST_PORT=8082
GHOST_DOCUMENT_ROOT=/mnt/data/pgns
GHOST_ROOT=hello-world.txt
```
## Filesystem watching
`gHost` supports watching the filesystem for changes
## Tutorials
### deploy React.js app using `gHost` and `create-react-app`
#### Get the React.js project
Install `npm` if you don't already have it
```bash
# Ubuntu
$ sudo apt install npm
# RHEL
$ sudo yum install npm
# Arch
$ sudo pacman -S npm
```
Install `create-react-app` if you don't already have it

```bash
$ npm install create-react-app
```

Create the the React.js project and change into it's directory

```
$ npx create-react-app ghost-tutorial-react
$ cd ghost-tutorial-react
```

Check to see if the project has been installed correctly by starting the development server
```bash
~/ghost-tutorial-react $ npm start
```

If the project is working you should see output similar to this in the console
```bash

You can now view ghost-tutorial-react in the browser.

  Local:            http://localhost:3000/
  On Your Network:  http://192.168.1.193:3000/

Note that the development build is not optimized.
To create a production build, use yarn build.

```
navigate to (http://localhost:3000/)[http://localhost:3000/] and if the app runs, you are all set to create a production build!

> you can stop the development server at any time by pressing `Control C`
> If you see yarn instead of npm don't worry, either one will work


#### Create a custom build
stop the development server and create a production build
```bash
~/ghost-tutorial-react $ npm run build
```

when the build is finished running you should have a `build` directory in your projects root, with all of the static assets that you need for your React.js app.
```bash
~/ghost-tutorial-react $ ls
build  node_modules  package.json  public  README.md  src  yarn.lock
```

#### Docker

We are now going to containerize our react app, and use docker to serve the static files

add the following `Dockerfile` to your project root
```dockerfile

```

and `docker-compose.yml`
```yaml
version: "3.7"

services:
  ghost-tutorial-react:
    image: ghost
    volumes:
      - ./build:/usr/src/app
    ports:
      - 8080:8080

```
#### Kubernetes
